package in.forsk.iiitk;

import com.androidquery.util.AQUtility;

import android.util.DisplayMetrics;

public class Application extends android.app.Application {

	public static int mDeviceWidth = 0;
	public static int mDeviceHeight = 0;

	@Override
	public void onCreate() {
		super.onCreate();

		AQUtility.setDebug(true);

		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		mDeviceWidth = displayMetrics.widthPixels;
		mDeviceHeight = displayMetrics.heightPixels;
	}

}
